
import {MongoClient} from 'mongodb'

const url = process.env.DATABASE_URL
const DATABASE_NAME = "testGolang"

export const getConnection = async () => {
    const client = await MongoClient.connect(url)
    const dbo = client.db(DATABASE_NAME);
    return Promise.resolve(dbo)
}