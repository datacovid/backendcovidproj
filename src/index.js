import 'dotenv/config'
const Discord = require("discord.js");
import {getConnection} from './database'
// Require the framework and instantiate it
const fastify = require('fastify')({ logger: true })
fastify.register(require('fastify-cors'), { 
    origin: true,
   allowedHeaders: ['Origin', 'X-Requested-With', 'Accept', 'Content-Type', 'Authorization'],
   methods: ['GET', 'PUT', 'PATCH', 'POST', 'DELETE']
  })
  
const client = new Discord.Client();
client.login(process.env.DISCORD_TOKEN);
 
let dbConnection = null
//GOTTA CLEAN THIS FILE
const startServer = async    () => {
    dbConnection = await getConnection()
    fastify.get('/', async (request, reply) => {
        const data = await dbConnection.collection("dateCol").find({}).toArray()
        return { data }
    })
    await fastify.listen(process.env.PORT, '0.0.0.0')

}
    
startServer()